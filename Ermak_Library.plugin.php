<?php
/*
Plugin Name: Ermak.Library
Plugin URI: http://glazun.com/?page_id=103
Description: Use WP media as Goods Batch
Version: 1.0.0
Date: 28.08.2015
Author: Genagl
Author URI: http://www.glazun.com/wordpress
License: GPL2
*/
/*  Copyright 2014  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful, 
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 
//библиотека переводов
function init_ermak_Manuscript_textdomain() { 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("ermak_manuscript", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
} 
// Вызываем ее до начала выполнения плагина 
add_action('plugins_loaded', 'init_ermak_Manuscript_textdomain');

//Paths
define('ERMAK_MANUSCRIPT_URLPATH', WP_PLUGIN_URL.'/Ermak_Library/');
define('ERMAK_MANUSCRIPT', 'ermak_manuscript');
define('ERMAK_MANUSCRIPT_TYPE', 'ermak_manuscript');
define('ERMAK_MANUSCRIPT_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');


require_once( ERMAK_MANUSCRIPT_REAL_PATH . 'class/Ermak_Manuscript_Main.class.php' );
require_once( ERMAK_MANUSCRIPT_REAL_PATH . 'class/Ermak_Manuscript_Assistants.php' );
require_once( ERMAK_MANUSCRIPT_REAL_PATH . 'class/Ermak_Manuscript.class.php' ); // КАК КОНВЕРТИРОВАТЬ В UTF-8 БЕЗ DOM?
require_once( ERMAK_MANUSCRIPT_REAL_PATH . 'shortcode/my_manuscripts.php' );



register_activation_hook( __FILE__, array( Ermak_Manuscript_Main, 'activate' ) );
if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array(Ermak_Manuscript_Main, 'deactivate'));
}

$Ermak_Manuscript_Main			= Ermak_Manuscript_Main::get_instance();
Ermak_Manuscript::init();

?>