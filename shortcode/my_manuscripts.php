<?php
	function my_manuscripts()
	{
		global $need_login_stroke;
		if(!is_user_logged_in())
		{
			echo $need_login_stroke;
			return;
		}
		$bts			= get_manuscripts_list();		
		return  $bts;
	}
	
	function get_manuscripts_list($only_mine = true, $is_store=false, $is_public=false)
	{		
		return Ermak_Manuscript_Main::get_table($only_mine, $is_store, $is_public);
		/*
		global $wb_query, $user_iface_color, $Soling_Metagame_Constructor;
		$html							= "";	
		
		$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'ASC',
										'post_type' 	=> ERMAK_MANUSCRIPT_TYPE,
										'post_status' 	=> 'publish'
									);
		if($only_mine)
		{
			$ow_locations_ids			= $Soling_Metagame_Constructor->all_user_locations();
			if(count($ow_locations_ids) == 0)	return "nothing";
			$ar['meta_query']			= array(
												array(
														'key'		=> 'owner_id',
														'value'		=> $ow_locations_ids,
														'operator'	=> "OR"
												)
											);
		}
		if($is_store)
		{
			$ar['meta_query']			= array(
												array(
														'key'		=> 'store',
														'value'		=> "1"
												)
											);
		}
		if($is_public)
		{
			$ar['meta_query']			= array(
												array(
														'key'		=> 'is_public',
														'value'		=> "2"
												)
											);
		}
		//insertLog("get_manuscripts_list 1", $ar);
		$hubs				= get_posts($ar);
		//insertLog("get_manuscripts_list 2", $hubs);
		$tab				= array();
		$i=0;
		foreach($hubs as $hub)
		{		
			$hub			= Ermak_Manuscript::get_instance($hub);
			$slide			.= Ermak_Manuscript::get_form($hub->id);
			$title			= $hub->body->post_title;
			if( Ermak_Manuscript::is_user_owner( $hub->id ) )
				$title		.= Assistants::get_short_your_label(23, array(-1, -1));
			$tab[]			= array( "title"=>$title, "slide"=> $i==0 ? $slide : "", "exec"=>"", "args"=>$hub->id.','. $i );
			$i++;				
		}
		$html				.= Assistants::get_lists($tab, "manuscripts_lists");
		return $html;
		*/
	}
?>