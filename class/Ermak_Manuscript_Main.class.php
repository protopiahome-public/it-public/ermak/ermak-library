<?php
	class Ermak_Manuscript_Main
	{
		static $instance;
		static function get_instance()
		{
			if(!static::$instance)
				static::$instance = new static;
			return static::$instance;
		}
		function __construct()
		{
			add_action( 'admin_menu', 					array(__CLASS__, 'admin_page_handler'), 11);
			add_filter( "eb_circle_menu_elements", 		array(__CLASS__, "add_cyrcle_menu_elements"), 11);
			add_action( 'admin_enqueue_scripts', 		array(__CLASS__, 'add_frons_js_script') );
			add_action( 'wp_enqueue_scripts', 			array(__CLASS__, 'add_frons_js_script') );
			add_action( 'smc_myajax_submit', 			array(__CLASS__, 'smc_myajax_submit'),15);
			add_action( 'smc_myajax_admin_submit', 		array(__CLASS__, 'smc_myajax_admin_submit'), 15);
			//add_action( 'admin_menu', 				array(__CLASS__, 'add_admin_menu_separator') );
			add_action("init", 							array(__CLASS__, "add_shortcodes"));
			
			add_action('wp_ajax_my_file_upload', 		array(__CLASS__, 'handle_file_upload'));
			add_action('wp_ajax_nopriv_my_file_upload', array(__CLASS__, 'handle_file_upload'));
		}
		
		static function handle_file_upload($params)
		{
			/*
			check_ajax_referer('ajax_file_nonce', 'security');
			if(!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX)){
				return;
			}
			*/
			if(!function_exists('wp_handle_upload')){
				require_once(ABSPATH . 'wp-admin/includes/file.php');
			}
			$response = array(Assistants::echo_me($_POST));
			$upload_overrides = array('test_form' => false);
			foreach($_FILES as $file)
			{
				$ID 	= media_handle_sideload($file, $upload_overrides);
				$src 	= wp_get_attachment_url( $ID );
				$nn		= array(
					"title"				=> $file['name'],
					"description"		=> $_POST['mnsc_descr'],
					"owner_id"			=> $_POST['nm_ow'], 
					"is_public"			=> $_POST['is_public'] == "true",
					"file_url"			=> $src,
					"price"				=> $_POST['a_price'],
					"exclusive_price"	=> $_POST['e_price'],
					"currency_type_id"	=> $_POST['nm_ct'],
					);
				Ermak_Manuscript::insert( $nn );
				$response['message'] = $nn;
			}
			echo json_encode($response);
			die();
		}
		
		
		
		
		
		static function add_shortcodes()
		{
			require_once(ERMAK_MANUSCRIPT_REAL_PATH."shortcode/my_test.php");
			add_shortcode('my_test', 	'my_test'); 
		}
		static function admin_page_handler()
		{
			add_menu_page( 
						__("Ermak.Library", ERMAK_MANUSCRIPT), // title
						__("Ermak.Library", ERMAK_MANUSCRIPT), // name in menu
						'manage_options', // capabilities
						'Ermak_Library_page', // slug
						array(__CLASS__, 'library_setting_pages'), // options function name
						ERMAK_MANUSCRIPT_URLPATH .'img/em_logo.png', 
						'20.402'
						);
		}
		static function library_setting_pages()
		{
			
		}
		static function activate()
		{
			global $wpdb;
			$wpdb->query(
					"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "ermak_location_manuscript` (
					  `location_id` int(11) NOT NULL,
					  `manuscript_id` int(11) NOT NULL,
					  UNIQUE KEY `locc_manu` (`location_id`,`manuscript_id`)
					) ENGINE=MyISAM DEFAULT CHARSET=cp1251;"
				);
			$wpdb->query(
					"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "ermak_manuscript_meta` (
					  `id` int(10) NOT NULL,
					  `xclusive_price` int(10) NOT NULL,
					  `price` int(10) NOT NULL,
					  `currency_type_id` int(10) NOT NULL,
					  `file_url` varchar(100) NOT NULL,
					  `owner_id` int(10) NOT NULL,
					  `is_public` tinyint(1) NOT NULL,
					  `is_patent` tinyint(1) NOT NULL,
					  `download_count` int(10) NOT NULL
					) ENGINE=MyISAM DEFAULT CHARSET=cp1251;"
				);
		}
		static function deactivate()
		{
			//global $wpdb;
			//$wpdb->query("DROP TABLE IF EXISTS `" . $wpdb->prefix . "ermak_location_manuscript`");
		}
		static function add_cyrcle_menu_elements($arr)
		{
			$arr[]		= array("picto"=> ERMAK_MANUSCRIPT_URLPATH."img/manuscript_ico.png", 	"hint"=> __("Library", ERMAK_MANUSCRIPT),	"slug"=>"manuscripts",		"exec" => "show_manuscript_win");
			return $arr;
		}
		
		static function add_frons_js_script()
		{			
			//css
			wp_register_style('ermak_manuscript', ERMAK_MANUSCRIPT_URLPATH . 'css/ermak_manuscript.css', array());
			wp_enqueue_style( 'ermak_manuscript');
			
			//js
			wp_register_script('e_manuscript_front_js', plugins_url( '../js/e_manuscript_front.js', __FILE__ ), array());
			wp_enqueue_script('e_manuscript_front_js');						
		}
		static function is_production()
		{
			return is_plugin_active('Ermak_production/Ermak_production.php');
		}
		static function is_finance()
		{
			if( !static::is_production() )	
					return false;
			return SolingMetagameProduction::is_finance();
		}
		static function add_admin_menu_separator()
		{
			//add_menu_page( '', '', 'read', 'wp-menu-separator', '', '', '21' );
			add_submenu_page( "Metagame_Production_page", 'wp-menu-separator', '', 'read', '5', '' );
		}
		static function get_load_form()
		{
			global $need_login_stroke, $Soling_Metagame_Constructor;
			if(!is_user_logged_in())
				return $need_login_stroke;
			if(static::is_production())
			{
				if(SolingMetagameProduction::is_finance())
				{
					$pay	= "
					<div class='manu_table_descr'>".__("Set access price", ERMAK_MANUSCRIPT) . "</div>
					<input type='number' min='0' value='0' class='smc_decor' style='width:120px!important;' id='a_price'/>
					<div class='manu_table_descr'>".__("Set exclusive price", ERMAK_MANUSCRIPT) . "</div>
					<input type='number' min='0' value='0' class='smc_decor' style='width:120px!important;' id='e_price'/>".
					SMP_Currency_Type::wp_dropdown(array("id"=>'nm_ct',  'style'=>'width:120px;'));
				}
			}
			$html = "
			<div class='load_form' style=''>
				<h3>".__("Insert new Manuscript", ERMAK_MANUSCRIPT) ."</h3>				
				<div class='file_upload'>
					<button class='button' type='button'>Выбрать</button>
					<div>" . __("choose file", ERMAK_MANUSCRIPT) . "</div>
					<input type='file'>
				</div>
				<textarea placeholder='" . __("insert description for file", ERMAK_MANUSCRIPT) . "' id='mnsc_descr'></textarea>
				<table>
					<tr>
						<td>
							<label class='smc_decor'>".__("Choose owner", ERMAK_MANUSCRIPT). "</label>
						</td>
						<td>
							<label class='smc_input'>".
								$Soling_Metagame_Constructor->wp_drop_location_by_user_owner( array("id" => "mnsc_ow")).
							"</label>
						<td>
					</tr>
					<tr>
						<td>
							<input type='checkbox' class='css-checkbox1' id='is_public'/>
							<label class='css-label1' style='color:black;' for='is_public'>".__("Is public", ERMAK_MANUSCRIPT). "</label>
						</td>
						<td>".
							$pay.
				"		</td>
					</tr>
				</table>
				<div class='button' id='upload_new_mnscr'>".
					__("Insert", ERMAK_MANUSCRIPT).
				"</div>
			</div>";
			return $html;
		}
		
		static function get_table($is_mine=false, $is_store=false, $is_public=false)
		{
			global $Soling_Metagame_Constructor, $need_login_stroke, $wpdb;
			Ermak_Manuscript::get_location_manuscript_user();
			if($is_mine)
			{
				if(!is_user_logged_in())
				{
					return $need_login_stroke;
				}
				$ow_locations_ids	= $Soling_Metagame_Constructor->all_user_locations();
				if(!count($ow_locations_ids))
					return 'nothing';
				$metas			= array("owner_id" => $ow_locations_ids);
				$slide_			= "mine";
				//Добавляем манускрипты, доступы к которым куплены юзером
				add_filter("smc_query_where", function($query)
				{
					$access		= Ermak_Manuscript::get_location_manuscript_user();
					if(count($access))
						$query	.= " OR pmeta.id IN (" . implode(",", Ermak_Manuscript::get_location_manuscript_user() ). ") ";
					//insertLog("get_table", $query);
					return $query;
				});
			}
			else if($is_store)
			{
				$metas			= array("store" => "1");
				$slide_			= "store";
			}
			else if($is_public)
			{
				$metas			= array("is_public" => "1");
				$slide_			= "all";
			}
			
			$cb			= Ermak_Manuscript::get_all($metas);
			if(count($cb) == 0)	return "<div class='smp-comment'>".__("No one", "smc")."</div>";
			$slide		.= "<table class='goods_type_tbl tb_manuscript' exec='show_manuscript_cont' args='$slide_, mnscrptc'>";
			foreach($cb as $c)
			{
				$gb		= Ermak_Manuscript::get_instance($c);
				$slide	.= $gb->get_table_row();				
				$i++;
			}
			$slide		.= "</table>";
			return $slide;
		}
		static function show_manuscript_table($int)
		{
			switch($int)
			{
				case "insert":
					$html		= static::get_load_form();
					break;
				case "mine":
					$html		= get_manuscripts_list(true);	
					break;	
				case "all":
					$html		= get_manuscripts_list(false, false, true);	
					break;
			}
			return $html;
		}
		static function smc_myajax_submit($params)		
		{	
			global $start, $Soling_Metagame_Constructor, $smc_height;
			$html			= 'none!';
			switch($params[0])
			{
				case 'show_manuscript_win':
					$html				= static::get_load_form();
					$title				= __("Library", ERMAK_MANUSCRIPT);
					$title_switcher		= array();
					$title_switcher[]	= array("title"=>__("Insert", ERMAK_MANUSCRIPT),	"exec"=>"show_manuscript_cont", "args"=>"insert,mnscrptc");
					$title_switcher[]	= array("title"=>__("only mine", "smp"),			"exec"=>"show_manuscript_cont", "args"=>"mine,mnscrptc");
					$title_switcher[]	= array("title"=>__("Is public", ERMAK_MANUSCRIPT), "exec"=>"show_manuscript_cont", "args"=>"all,mnscrptc");
					$d					= array(	
													$params[0], 
													array(
															'text'	=> Ermak_Float_Widget::add_window(array('id'=>'mnscrptc', 'content'=>$html, "title"=> $title, "width" => 500, "height"=> 650, "title_switcher"=>$title_switcher  )),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'show_manuscript_cont':
					$goods_type_id		= $params[1];
					$html				= static::show_manuscript_table($params[1][0]);
									
					$d					= array(	
													$params[0], 
													array(
															'text'		=> $html,
															'cont'		=> $goods_type_id[1],
															'time'		=> ( getmicrotime()  - $start )															
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case "show_manuscript":
					$id					= $params[1];
					//$manu				= Ermak_Manuscript::get_instance($id);
					$d					= array(	
													$params[0], 
													array(
															//'text'		=> $manu->get_table_row();,
															'time'		=> ( getmicrotime()  - $start ),
															'a_alert'	=>  Ermak_Manuscript::get_form($id)
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'update_manuscript':
					$data				= $params[1];
					$id					= (int)$data['id'];
					wp_update_post(array("ID"=> $id, "post_content" => $data['content']));
					$manu				= Ermak_Manuscript::get_instance($id);					
					$manu->update_metas(
						array(
							"is_public"			=> $data['pb'] == "true" ? 1 : 0,
							"currency_type"		=> $data['ctid'],
							"price"				=> $data['pr'],
							"exclusive_price"	=> $data['epr']
						)
					);
					$d					= array(	
													$params[0], 
													array(
															//'text'		=> $html,
															'time'		=> ( getmicrotime()  - $start ),
															'a_alert'	=> Ermak_Manuscript::get_form($id)
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'download_mnscrpt':
					$mid				= $params[1];
					$manuscript			= Ermak_Manuscript::get_instance($mid);					
					if( is_user_logged_in() && $manuscript->is_mine() )
					{
						$html			= $manuscript->do_download();
					}
					else
					{
						$alert			= __("Error!");
					}
					$d					= array(	
													$params[0], 
													array(
															//'text'		=> $html,
															'time'		=> ( getmicrotime()  - $start ),
															'a_alert'	=> $alert
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case "get_transer_manuscript_form":
					$id				= $params[1];
					$mns			= Ermak_Manuscript::get_instance($id);
					$title			= __("Transfer Manuscript", ERMAK_MANUSCRIPT);
					$html			= $mns->get_transfer_form();
					$d				= array(	
											$params[0], 
											array(
													'a_alert'			=> $html,
													'a_title'			=> $title,
												 )
										);
					$d_obj			= json_encode($d);
					print $d_obj;
					break;
				case "transfer_manuscript":
					$id				= $params[1];
					$new_ow_id		= $params[2];	
					$int			= $params[3];
					$mns			= Ermak_Manuscript::get_instance($id);
					if($mns->is_mine())
					{
						$html		= __("Successfull", ERMAK_MANUSCRIPT);
						$mns->update_metas( array( "owner_id" => $new_ow_id ) );
						$exec		= "close_modal";
					}
					else
					{
						$html		= __("This Manuscript is not your! Call Master right now!", ERMAK_MANUSCRIPT);
						$exec		= "close_modal";
					}
					$title			= __("Transfer Manuscript", ERMAK_MANUSCRIPT);
					$table			= static::show_manuscript_table($int[0]);					
					$d				= array(	
											$params[0], 
											array(
													'a_alert'			=> $html,
													'a_title'			=> $title,
													"text"				=> $table,
													"exec"				=> $exec,
													'cont'				=> $int[1],
												 )
										);
					$d_obj			= json_encode($d);
					print $d_obj;
					break;
				case "pay_manuscript_form":
					$id				= $params[1];
					$mns			= Ermak_Manuscript::get_instance($id);
					$title			= __("Pay exclusive Manuscript", ERMAK_MANUSCRIPT);
					$html			= $mns->get_full_pay_form();
					$d				= array(	
											$params[0], 
											array(
													'a_alert'			=> $html,
													'a_title'			=> $title,
												 )
										);
					$d_obj			= json_encode($d);
					print $d_obj;
					break;
					
				case "pay_exclusive_manuscript":
					$id					= $params[1];
					$acc_id				= $params[2];	
					$int				= $params[3];
					$isac_owner			= SMP_Account::is_user_owner($acc_id);
					if($isac_owner)
					{				
						$acc			= SMP_Account::get_instance($acc_id);
						$mnscrpt		= Ermak_Manuscript::get_instance($id);
						$ct_id			= $mnscrpt->get_meta("currency_type_id");
						$ct				= SMP_Currency_Type::get_instance($ct_id);
						$price			= $mnscrpt->get_meta("exclusive_price");
						$mn_owner_id 	= $mnscrpt->get_meta("owner_id");
						$owner_id		= $acc->get_owner_id();
						$loc			= SMC_Location::get_instance($owner_id);
						$summ			= SMP_Account::see_summ_location( $owner_id, $acc->get_currency_type_id() );
						$need_summ		= $price;
						if($summ > $need_summ)
						{							
							$html		= "Success";
							$title		= __("Success", "smc");
							$exec		= "close_modal";
							$reason		= sprintf(
								__("Payment for the purchase of consignment manuscript ID=%s, %s", ERMAK_MANUSCRIPT), 
								"<b>" . $id . "</b>",
								"<b>" . $mnscrpt->get("post_title") . "</b>"
							);
							$reason2	= sprintf(
								__("Location %s consignment of Manuscript %s.", ERMAK_MANUSCRIPT),
								"<b>" . $loc->name . "</b>", 
								"<b>" . $id . "</b>"
							);
							$mnscrpt->update_metas(array("owner_id" => $owner_id));
							SMP_Account::remove_summ_from_location( $owner_id, $acc->get_currency_type_id(), $need_summ, $reason );
							SMP_Account::add_summ_to_location($mn_owner_id, $acc->get_currency_type_id(), $need_summ, $acc_id, $reason2 );
							
							/**/
						}
						else
						{
							$html		= sprintf( __("You don't have the required amount in your Accounts in the required currency. It should be - %s, and there are - %s.", "smp"), "<b>" . $summ . "</b>",  "<b>" . $need_summ . "</b>" );
							$title		= __("Error", "smc");
						}
					}
					else
					{
						$html		= __("You not owner of this account! Call Master.", "smp");
						$title		= __("Error", "smc");
					}	
					$table			= static::show_manuscript_table($int[0]);
					/*
						*/
					$d				= array(	
											$params[0], 
											array(
													'a_title'		=> $title,
													'a_alert'		=> $html,
													"exec"			=> $exec,
													"text"			=> $table,
													'cont'			=> $int[1],
												 )
										);
					$d_obj			= json_encode($d);
					print $d_obj;
					//insertLog("pay_exclusive_manuscript", $d_obj);
					break;
				case "access_manuscript_form":
					$id				= $params[1];
					$mns			= Ermak_Manuscript::get_instance($id);
					$title			= __("Pay exclusive Manuscript", ERMAK_MANUSCRIPT);
					$html			= $mns->get_access_pay_form();
					$d				= array(	
											$params[0], 
											array(
													'a_alert'			=> $html,
													'a_title'			=> $title,
												 )
										);
					$d_obj			= json_encode($d);
					print $d_obj;
					break;
				case "pay_access_manuscript":
					$id					= $params[1];
					$acc_id				= $params[2];	
					$int				= $params[3];
					$isac_owner			= SMP_Account::is_user_owner($acc_id);
					if($isac_owner)
					{				
						$acc			= SMP_Account::get_instance($acc_id);
						$mnscrpt		= Ermak_Manuscript::get_instance($id);
						$ct_id			= $mnscrpt->get_meta("currency_type_id");
						$ct				= SMP_Currency_Type::get_instance($ct_id);
						$price			= $mnscrpt->get_meta("price");
						$mn_owner_id 	= $mnscrpt->get_meta("owner_id");
						$owner_id		= $acc->get_owner_id();
						$loc			= SMC_Location::get_instance($owner_id);
						$summ			= SMP_Account::see_summ_location( $owner_id, $acc->get_currency_type_id() );
						$need_summ		= $price;
						if($summ > $need_summ)
						{		
							$reason		= sprintf(
								__("Payment for access manuscript ID=%s, %s", ERMAK_MANUSCRIPT), 
								"<b>" . $id . "</b>",
								"<b>" . $mnscrpt->get("post_title") . "</b>"
							);
							$reason2	= sprintf(
								__("Location %s consignment access of Manuscript %s.", ERMAK_MANUSCRIPT),
								"<b>" . $loc->name . "</b>", 
								"<b>" . $id . "</b>"
							);
							$succ		= $mnscrpt->add_location_access($owner_id);
							SMP_Account::remove_summ_from_location( $owner_id, $acc->get_currency_type_id(), $need_summ, $reason );
							SMP_Account::add_summ_to_location($mn_owner_id, $acc->get_currency_type_id(), $need_summ, $acc_id, $reason2 );					
							$html		= $succ ? __("Success", "smc"): __("no successfull!!!!!!!", "smc" );
							$title		=  __("Success", "smc");
							$exec		= "close_modal";
							
							/**/
						}
						else
						{
							$html		= sprintf( __("You don't have the required amount in your Accounts in the required currency. It should be - %s, and there are - %s.", "smp"), "<b>" . $summ . "</b>",  "<b>" . $need_summ . "</b>" );
							$title		= __("Error", "smc");
						}
					}
					else
					{
						$html		= __("You not owner of this account! Call Master.", "smp");
						$title		= __("Error", "smc");
					}	
					$table			= static::show_manuscript_table($int[0]);
					/*
						*/
					$d				= array(	
											$params[0], 
											array(
													'a_title'		=> $title,
													'a_alert'		=> $html,
													"exec"			=> $exec,
													"text"			=> $table,
													'cont'			=> $int[1],
												 )
										);
					$d_obj			= json_encode($d);
					print $d_obj;
					//insertLog("pay_exclusive_manuscript", $d_obj);
					break;
			}
		}
		static function smc_myajax_admin_submit($params)
		{
			global $start, $Soling_Metagame_Constructor, $smc_height;
			$html			= 'none!';
			switch($params[0])
			{
				case 'upload_manuscript_media':
					$html				= $params[1];					
					$d					= array(	
													$params[0], 
													array(
															'text'		=> Ermak_Manuscript::get_admin_form($html['url'], $html['filename']),
															'filename'	=> $html['filename'],
															"url"		=> $html['url'],
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
			}
		}
	}
?>