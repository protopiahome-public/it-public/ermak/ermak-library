var show_manuscript_cont = function(){};

jQuery( document ).ready(function( $ ) 
{  
	//switch's buttons on window title in float window
	show_manuscript_cont = function(params)
	{
		var p	= params.split(',');
		send(['show_manuscript_cont', p], false);
	}
	
	$( "#ema_upload_modal" ).live({click:function() 
	{
		on_insert_media = function(json)
		{
			send(['upload_manuscript_media', json], false);
		}
		open_media_uploader_image();						
	}});
	//download button
	$(".mnsdn").live({click:function(evt)
	{
		var mnsdn	= $(this).attr("mid");
		send( ['download_mnscrpt', mnsdn], false );
	}})
	
	$(".show_mnsdn").live({click:function(evt)
	{
		var mid	= $(this).attr("mid");
		send( ['show_manuscript', mid], false );
	}})
	
	// pay manuscript
	$(".pay_manuscript").live({click:function(evt)
	{
		send(["pay_manuscript_form", $(this).attr("mid")], false);	
	}});
	$("#full_pay_manuscript").live({click:function(evt)
	{
		var sl			= $(this).closest("[mid]");
		var mid			= sl.attr("mid");
		var acc_id		= sl.find("#pmnscr_acc_id").val();
		var slide		= $(".tb_manuscript").attr("args");
		var p			= slide.split(',');
		send(["pay_exclusive_manuscript", mid, acc_id, p]);		
	}});
	$(".pay_access_manuscript").live({click:function(evt)
	{
		var mid			= $(this).attr("mid");
		send(["access_manuscript_form", mid], false);		
	}});
	$("#access_pay_manuscript").live({click:function(evt)
	{
		var sl			= $(this).closest("[mid]");
		var mid			= sl.attr("mid");
		var acc_id		= sl.find("#pmnscr_acc_id").val();
		var slide		= $(".tb_manuscript").attr("args");
		var p			= slide.split(',');
		send(["pay_access_manuscript", mid, acc_id, p]);		
	}});
	
	$(".mnstrnsfr").live({click:function(evt)
	{
		var mid			= $(this).attr("mid");
		//a_alert(mid);
		send(['get_transer_manuscript_form', mid], false);
	}});
	$("#transfer_manuscript").live({click:function(evt)
	{
		var sl			= $(this).closest("[mid]");
		var mid			= sl.attr("mid");
		var now_id		= sl.find("#new_owner_id").val();
		if(now_id == -1 || now_id == "")
		{
			a_alert(__("Select owner"));
			return;
		}
		var slide		= $(".tb_manuscript").attr("args");
		console.log(slide);
		var p			= slide.split(',');
		send(["transfer_manuscript", mid, now_id, p], true);
	}});
	
	//file uploader
	var wrapper, 
		wrapper = $( ".file_upload" ),
        inp = wrapper.find( "input" ),
        btn = wrapper.find( "button" ),
        lbl = wrapper.find( "div" );
	var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
    inp.live({change:function()
	{
        var file_name;
		wrapper = $( ".file_upload" ),
        inp = wrapper.find( "input" ),
        btn = wrapper.find( "button" ),
        lbl = wrapper.find( "div" );
        if( file_api && inp[ 0 ].files[ 0 ] )
            file_name = inp[ 0 ].files[ 0 ].name;
        else
            file_name = inp.val().replace( "C:\\fakepath\\", '' );

        if( ! file_name.length )
            return;

        if( lbl.is( ":visible" ) ){
            lbl.text( file_name );
            btn.text( "Выбрать" );
        }else
            btn.text( file_name );
    }})
	$( window ).resize(function(){
		$( ".file_upload input" ).triggerHandler( "change" );
	});
	
	//NEW MANUSCRIPT DOWNLOAD
	var is_public = 0, 
		a_price = 0, 
		e_price = 0, 
		nm_ct = 0,
		mnsc_descr = "",
		mnsc_ow = -1;
	$("#is_public").live({change:function(evt)
	{
		is_public	= $(this).val();	
	}});
	$("#a_price").live({change:function(evt)
	{
		a_price		= $(this).val();
	}});
	$("#e_price").live({change:function(evt)
	{
		e_price		= $(this).val();
	}});
	$("#nm_ct").live({change:function(evt)
	{
		nm_ct		= $(this).val();		
	}});
	$("#mnsc_descr").live({change:function(evt)
	{
		mnsc_descr	= $(this).val();		
	}});
	$("#mnsc_ow").live({change:function(evt)
	{
		mnsc_ow		= $(this).find('option:selected').val();
	}});
	$("#upload_new_mnscr").live({click:function(evt)
	{
		if($('.file_upload input')[0].files.length==0)
		{
			a_alert(__("Upload file please"));
			return;
		}
		if(mnsc_ow == -1)
		{
			a_alert(__("Please select owner."));
			return;
		}
		if(mnsc_descr == "")
		{
			a_alert(__("Insert description please"));
			return;
		}
		var formData	 	= new FormData();
        jQuery.each($('.file_upload input')[0].files, function(i, file) 
		{
			formData.append('uploadFile-'+i, file);			
        });
		nm_ct		= nm_ct == 0 ? $("#nm_ct").attr("value") : nm_ct;
		formData.append('is_public', is_public == "on");
		formData.append('a_price', a_price);
		formData.append('e_price', e_price);
		formData.append('nm_ct', nm_ct);
		formData.append('nm_ow', mnsc_ow);
		formData.append('mnsc_descr', mnsc_descr);
        formData.append('action', 'my_file_upload');
		get_muar();
		$(".smc_muar").fadeIn("fast");
		interval		= setInterval(function()
		{
			clearInterval(interval);
			$(".smc_muar").fadeOut('slow').detach();
			$("#refresher").hide();
			$("#lp_dm_owner").hide();
		}, 15000);
		
		
		jQuery.ajax({
            url: myajax.url,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            //MODIFIED - From ajaxData to formData
            data: formData,
            beforeSend: function(jqXHR, settings){
                //console.log("Haven't entered server side yet.");
            },
            dataFilter: function(data, type){
                //May need/want to create a function to parse the data. Which includes
                // PHP Errors that would normally break the AJAX function with a JS 
                // Error with no sign of the PHP Error message. Normally want to 
                // try/catch php errors.
               // console.log("JSON string echoed back from server side.");
            },
            success: function(data, textStatus, jqXHR){
               // console.log("Back from server-side!");
                //Checking errors that may have been caught on the server side that
                // normally wouldn't display in the error Ajax function.
               console.log( jqXHR.responseText);
			   if(is_muar)	clear_muar();
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log("A JS error has occurred.");
            },
            complete: function(jqXHR, textStatus){
				//console.log(jqXHR, textStatus);
            }
        });  
    }});
	$(".updmnscrfrm").live({click:function(evt)
	{
		var data		= {}
		var sl			= $(this).closest("[mid]");
		data.id			= sl.attr("mid");	
		data.ctid		= sl.find("#nm_ct").attr("value");
		data.pr			= sl.find("#pr").attr("value");
		data.epr		= sl.find("#epr").attr("value");
		data.content	= sl.find("#content").attr("value");
		data.pb			= sl.find("[name=public]").prop("checked");
		console.log(data);
		close_modal();
		send(['update_manuscript',data], false);
	}}); 
});

document.documentElement.addEventListener("_send_", function(e) 
{	
	( function( $ )
	{
		var dat			= e.detail;
		var command		= dat[0];
		var datas		= dat[1];
		//console.log(command);
		switch(command)
		{
			case "show_manuscript_win":
				$("body").append(datas['text']);
				$(".eb_cbutton img").fadeIn('slow');
				$(".eb_cbutton #smc_wait_spin").detach();
				break;
			case 'show_manuscript_cont':
				$("[slug=" + datas['cont'] + "] .eb_float_cont").empty().append($(datas['text']).hide().fadeIn("slow"));
				break;
			case "download_mnscrpt":
			case "show_manuscript":
				break;
			case "update_manuscript":
				break;
			case "pay_exclusive_manuscript":
			case "pay_access_manuscript":
			case "transfer_manuscript":
				$("[slug=" + datas['cont'] + "] .eb_float_cont").empty().append($(datas['text']).hide().fadeIn("slow"));
				break;
			
		}
	})(jQuery);
})

document.documentElement.addEventListener("_send_admin_", function(e) 
{
	( function( $ )
	{
		var dat			= e.detail;
		var command		= dat[0];
		var datas		= dat[1];
		console.log(command);
		switch(command)
		{
			case "upload_manuscript_media":
				$("input[name='post_title']").val(datas['filename']).attr("placeholder", "");
				$("#title-prompt-text").text("");
				$("#upload_form").hide();
				$("[name=ema_upload_url]").val(datas['url']);
				$("[name=file_url]").val(datas['url']);
				$("#source_form").empty().append(datas['text']);				
				$("#source_form_cont").fadeIn("slow");				
				break;
		}
		if(datas['funct'])
		{
			window[datas['funct']](datas['funct_args']);
		}
	})(jQuery);
});


